#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 19 08:49:18 2019

@author: jessicapointing
"""

r"""
Max Clique
=============================

**Module name:** :mod:`gbsapps.graph.max_clique`

.. currentmodule:: gbsapps.graph.max_clique

The frontend module for users to find max cliques. The :func:`find_max_cliques` function
finds max cliques in a graph, which is NP-hard. 

Summary
-------

.. autosummary::
    find_max_clique

Code details
------------
"""
import gbsapps.sample as samp
import gbsapps.graph.graph_sample as g_samp
import gbsapps.graph.dense_subgraph as dense
import numpy as np
import networkx as nx

"""
Find the maximum clique in a graph

Args:
    graph (graph_type): the input graph
    number_of_nodes (int): the size of desired dense subgraph

Returns:
    graph: the maximum clique found
"""
def find_max_clique(graph, n_nodes):
    # Generate dense subgraph from Gaussian Boson Sampling
    dense_subgraph = dense.find_dense_subgraph(graph=graph, number_of_nodes=n_nodes, iterations=1, method="random-search")
    # Obtain the subgraph
    subgraph = graph.subgraph(dense_subgraph[1])
    # Check if the subgraph is a clique; if so, return that as the max clique
    if is_clique(subgraph):
        return subgraph
    else:
        # Flag that we have not found a clique
        not_found_clique = True
        # Copy the graph
        new_subgraph = nx.Graph(subgraph)
        # Whilst we have not found a clique
        while not_found_clique:
            # Find all the degrees of the vertices
            degrees = new_subgraph.degree()
            # Remove the vertex with the lowest degree
            vertex_to_remove = min(degrees, key = lambda t: t[1])[0]
            # Copy the graph
            new_subgraph_copy = nx.Graph(new_subgraph)
            # Remove the nodes from the copy
            new_subgraph_copy.remove_node(vertex_to_remove)
            # Copy the graph with the removed nodes
            new_subgraph = nx.Graph(new_subgraph_copy)
            # If the resulting graph is a clique, return it
            if is_clique(new_subgraph):
                not_found_clique = False
                return new_subgraph

"""
Check if a graph is a clique

Args:
    graph (graph_type): the input graph
    
Returns:
    bool: whether it is a clique or not
"""  
def is_clique(graph):
    # Retrieve number of edges in graph
    number_of_edges = graph.number_of_edges()
    # Retrieve number of vertices in graph
    number_of_vertices = graph.number_of_nodes()
    # Check that the condition holds for a clique
    condition = number_of_vertices*(number_of_vertices-1)/2
    return number_of_edges == condition
