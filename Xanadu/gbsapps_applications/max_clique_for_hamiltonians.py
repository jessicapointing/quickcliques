#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 19 08:58:17 2019

@author: jessicapointing
"""

import gbsapps
import numpy as np
import networkx as nx
import optimize_hamiltonians
import max_clique
import min_clique_cover

r"""
Max Clique for Hamiltonians
=============================

How to reduce the number of terms in Hamiltonian

Code details
------------
"""

hamiltonian = {0: ['i','i'],
               1: ['i','z'],
               2: ['z','i'],
               3: ['z','z'],
               4: ['x','x']}

adj_matrix = optimize_hamiltonians.get_adj_matrix_from_hamiltonian(hamiltonian)
print(adj_matrix)

graph = nx.Graph(adj_matrix)

max_clique = max_clique.find_max_clique(graph, graph.number_of_nodes())
min_clique = min_clique_cover.find_min_clique_cover(graph)
