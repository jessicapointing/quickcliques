#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 19 09:09:23 2019

@author: jessicapointing
"""

r"""
Optimize Hamiltonians
=============================

**Module name:** :mod:`gbsapps.applications.optimize_hamiltonians`

.. currentmodule:: gbsapps.applications.optimize_hamiltonians

The frontend module for users to optimize hamiltonians. This uses the minimum clique cover to
reduce the number of measurement of terms in the hamiltonian.

Summary
-------

.. autosummary::
    find_max_clique

Code details
------------
"""

import numpy as np
import networkx as nx
import max_clique

"""
Find the maximum clique in a graph

Args:
    hamiltonian (graph_type): the input hamiltonian

Returns:
    matrix: the adj matrix
"""
def get_adj_matrix_from_hamiltonian(Hamiltonian):
    # Initiliaze an adjacency matrix with zeroes
    adj_matrix = [[0 for j in range(len(Hamiltonian))] for i in range(len(Hamiltonian))]
    for term_index1, term1 in Hamiltonian.items():
        for term_index2, term2 in Hamiltonian.items():
            if term_index1 != term_index2:
                # Find the qubit-wise commuting between the terms
                if qwc(term1, term2):
                    # If the terms are qwc, then add a 1 to the adj matrix
                    adj_matrix[term_index1][term_index2] = 1
                    adj_matrix[term_index2][term_index1] = 1
    return np.matrix(adj_matrix)

"""
Qubit-Wise Commuting Terms

Args:
    term1, term2

Returns:
    bool
"""
def qwc(term1, term2):
    for i in range(len(term1)):
        if term1[i] != term2[i]:
            if term1[i] != 'i' and term2[i] != 'i':
                return False
    return True   