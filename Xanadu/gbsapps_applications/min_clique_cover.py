#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 19 08:49:18 2019

@author: jessicapointing
"""

r"""
Min Clique
=============================

**Module name:** :mod:`gbsapps.graph.min_clique_cover`

.. currentmodule:: gbsapps.graph.min_clique_cover

The frontend module for users to find min clique covers. The :func:`find_min_clique_cover` function
finds min clique covers in a graph, which is NP-hard. 

Summary
-------

.. autosummary::
    find_min_clique_cover

Code details
------------
"""
import gbsapps.sample as samp
import gbsapps.graph.graph_sample as g_samp
import gbsapps.graph.dense_subgraph as dense
import max_clique
import numpy as np
import networkx as nx

"""
Find the minimum clique cover in a graph

Args:
    graph (graph_type): the input graph
    number_of_nodes (int): the size of desired dense subgraph

Returns:
    graph: the minimum clique covers found
"""
def find_min_clique_cover(graph):
    # copy the graph
    new_graph = nx.Graph(graph)
    # Initalize list of minimum clique covers (mcc)
    mcc = []
    # Flag whether vertices still exist in graph
    vertices_exist = True
    while vertices_exist:
        # If there is only one node, add that as a min clique cover
        if new_graph.number_of_nodes() == 1:
            mcc.append(new_graph)
            return mcc
        if new_graph.number_of_nodes() > 1:
            n_nodes = new_graph.number_of_nodes()
            subgraph = max_clique.find_max_clique(new_graph, n_nodes)
            mcc.append(subgraph)
            new_graph2 = nx.Graph(new_graph)
            new_graph2.remove_nodes_from(subgraph.nodes)
            new_graph = nx.Graph(new_graph2)
        else:
            vertices_exist = False
    return mcc
